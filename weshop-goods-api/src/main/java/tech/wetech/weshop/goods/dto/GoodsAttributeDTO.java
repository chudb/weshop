package tech.wetech.weshop.goods.dto;

import lombok.Data;

@Data
public class GoodsAttributeDTO {

    private String name;

    private String value;
}
